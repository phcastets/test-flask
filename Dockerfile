# Set up Python 3.7 Alpine image
FROM python:3.7

# Define the environment variable STRINGS
ENV STRINGS 'ab ab abc'

# Define the directory of the image filesystem
WORKDIR /usr/src/app  

# Copy the files from host to image filesystem
COPY . .

# Add Flask
RUN pip install -r requirements.txt

# Launch the Python script
ENTRYPOINT ["python"]
CMD ["main.py"]



