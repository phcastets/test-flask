# SparseArray

SparseArray counts the number of time each query within a list of queries appears inside a predefined list of strings.


### Prerequisites 

* [Docker](https://docs.docker.com/docker-for-windows/install/) is  necessary to run SparseArray.

* The strings list can be found and modified in the Dockerfile if necessary. It is defined as an environment variable.

```bash
ENV STRINGS 'ab ab abc'
``` 
* The queries list is passed in the Swagger UI interface queries frame. For example:

```bash
ab,abc,bc
``` 


## Running the tests

Once Docker is installed, we can run SparseArray from the terminal:

**1. Change the working directory**

```bash
cd SparseArray_flask
``` 

**2. Create the image from the dockerfile and tags the container with the name "test" and label first**

```bash
docker build -t test:first .
```
**3. Create and start the container with port 5000**

```bash
docker run -p 5000:5000 test:first 
```
**4. Follow the URL http://0.0.0.0:5000/ and write the query**


**N.B The host is defined in the script main.py and the query written in the interface is ab,abc,bc**

The output is a json returning the number of time each query within the list of queries appears inside the predefined list of strings. With the current parameters, we obtain:
**{"ab": 2, "abc": 1,  "bc": 0}**. 


## Author

**Paul-Henri Castets** 