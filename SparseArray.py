import os
import sys

class SparseArray:
    '''Counts the number of time each query within a list of queries appears inside a predefined list of strings'''

    def __init__(self):
        # Make a list of strings from the environment variable STRING
        self.strings = os.environ.get('STRINGS').split(' ')
        
    def matching_strings(self, queries):
        # Initialize the count of each query at 0
        queries = queries.split(',')
        self.results = {q:0 for q in queries}
        # Add 1 to the count each time the query is found among the strings
        for q in queries:
            for s in self.strings:
                if q==s:
                    self.results[q]+=1
        return self.results

    
    def sizes(self):
        # Compute the size of each list
        self.queries_size = len(self.queries)
        self.strings_size = len(self.strings)
        
        # Print the size of each list
        size_message = print('The length of the queries list is %d \nThe length of the strings list is %d' 
                             %(self.queries_size, self.strings_size))
        return size_message 