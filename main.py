from flask import Flask, jsonify
from flask_restplus import Api, Resource
from SparseArray import SparseArray

# Define app as Flask app
app = Flask(__name__)

#Initialize API
api = Api(app)

# Heading in Swagger 
name_space = api.namespace('SparseArray') 

# URL defined through route:
@name_space.route("/<queries>")

# Define all endpoints under a given route inside a class
class MainClass(Resource):
    def get(self, queries):
        try:
            run = SparseArray()
            dictionary = run.matching_strings(queries)
            json_output = jsonify(dictionary)
            return json_output
        except Exception as e:
            print(str(e))
            return jsonify(str(e))

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')